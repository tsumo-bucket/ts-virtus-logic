@extends('layouts.admin')

@section('breadcrumbs')
<nav aria-label="breadcrumb" >
  <ol class="breadcrumb">
    <li class="breadcrumb-item far" ><a href="{{route('adminDashboard')}}">Dashboard</a></li>
    <li class="breadcrumb-item far active"><span>Profile</span></li>
  </ol>
</nav>
@stop

@section('header')
<header class="flex-center">
	<h1>{{ $title }}</h1>
	<div class="header-actions">
	<div class="header-actions">
		<a href="{{ route('adminProfileEdit') }}" class="caboodle-btn caboodle-btn-large caboodle-btn-cancel mdc-button" data-mdc-auto-init="MDCRipple">Edit</a>
		<a href="{{ route('adminProfilePasswordEdit') }}" class="caboodle-btn caboodle-btn-large caboodle-btn-cancel mdc-button" data-mdc-auto-init="MDCRipple">Change Password</a>
	</div>
</header>
@endsection

@section('content')
	<div class="row">
		<div class="col-sm-12">
			<div class="caboodle-card">
				{{-- <div class="caboodle-card-header">
					<h4 class="no-margin">User Details</h4>
				</div> --}}
				<div class="caboodle-card-body">
					<table class="caboodle-table">
						<tbody>
							<tr>
								<td width="15%">Name</td>
								<td class="sub-text-1">{{$user->name}}</td>
							</tr>
							<tr>
								<td width="15%">Email</td>
								<td class="sub-text-1">{{$user->email}}</td>
							</tr>
							<tr>
								<td width="15%">CMS</td>
								<td class="sub-text-1">@if($user->status) Yes @else No @endif</td>
							</tr>
							<tr>
								<td width="15%">Verfied</td>
								<td class="sub-text-1">@if($user->status) Verified @else Unverified @endif</td>
							</tr>
							<tr>
								<td width="15%">Status</td>
								<td class="sub-text-1">@if($user->status) Active @else Inactive @endif</td>
							</tr>
							<tr>
								<td width="15%">Type</td>
								<td class="sub-text-1">{{ucwords($user->type)}}</td>
							</tr>
							<tr>
								<td width="15%">Last Login</td>
								<td class="sub-text-1">{{ date('F j, Y \a\t H:i A', strtotime($user->last_login)) }}</td>
							</tr>
							<tr>
								<td width="15%">Last IP Address</td>
								<td class="sub-text-1">{{$user->last_ip}}</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
@endsection
